const mongoose = require('mongoose');

const articleSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, required: true, maxlength: 200, text: true},
    category: [{ type: String, required: false}],
    content : { type: String, required: true },
    thumbnail: { type: String, required: true}
});


module.exports = mongoose.model('Article', articleSchema);