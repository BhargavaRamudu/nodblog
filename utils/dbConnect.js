const mongoose = require('mongoose');

module.exports = () => {

    mongoose.connect(process.env.MONGO_URL,{ useNewUrlParser: true })
            .then(result => {
                console.log('connected to mongodb');
            })
            .catch(err => {
                console.log(err);
            });
        
}
