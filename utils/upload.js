// options multer, formidable , multiparty, busboy

const multer = require('multer');

// function fileFilter (req, file, cb) {
//     if(req.file === 'image/jpeg' || req.file === 'image/png' || req.file === 'image/jpg'){
//         cb(null, true);
//     } else {
//         cb(null, false);
//     }
// }

// const storage = multer.diskStorage({
//     destination: function(req, file, cb) {
//         cb(null, 'uploads/');
//     },
//     filename: function(req, file, cb) {
//         cb(null, `${Date.now()}-file.fieldname`);
//     },
    
// })

// module.exports = multer({
//     storage: storage,
//     fileFilter: fileFilter,
//     limits: {
//         fileSize: 1024 * 1024 *10
//     }
// })

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
})

const fileFilter = (req, file ,cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
        cb(null, true);
    } else {
        cb(null, false);
    }
        
}


// module.exports = multer({dest: 'uploads/'});
module.exports = multer({ storage: storage,
        limits: {
            fileSize: 1024 * 1024 * 5
        }
    });