const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');


router.post('/signup', (req, res, next) => {
    User.find({email: req.body.email })
        .exec()
        .then(user => {
            if(user.length >= 1) {
                return res.status(409).json({
                    message: 'can\'t process the request'
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if(err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                            
                        })
                        user
                            .save()
                            .then(result => {
                                res.status(201).json({
                                    message: 'user created successfully',
                                    
                                });
                                console.log(result);
                            })
                            .catch(err => {
                                
                                res.status(500).json({
                                    error: err
                                })
                            })
                    }
                });
            }
        })
        .catch(err => {
            console.log(err);
        })
    
});


router.post('/login', (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if(user.length < 1) {
                return res.status(404).json({
                    message: 'mail or password is incorrect'
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if(err) {
                    return res.status(401).json({
                        error: 'mail or password is incorrect',
                        
                    });
                }
                if(result) {

                    const token = jwt.sign(
                        {
                            email: user[0].email,
                            userId: user[0]._id
                        },
                        'secret',
                        {
                            expiresIn: "1h"
                        }
                    );
                    return res.status(200).json({
                        message: 'login success',
                        token: token
                    });
                }
                res.status(401).json({
                    error: 'you are not authorized'
                });
            })
        })
        .catch(err => {
                                
            res.status(500).json({
                error: err
            })
        })
})



router.delete("/:userId", (req, res, next) => {
    User.remove({_id: req.params.userId})
    .exec()
    .then(result => {
        res.status(200).json({
            message: "user deleted successfully"
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
});


module.exports = router;