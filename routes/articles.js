const Article = require('../models/article');
const router = require('express').Router();
const mongoose = require('mongoose');
const {checklogin} = require('../utils/check-login');

const upload = require('../utils/upload');

router.get('/', (req, res, next) => {
	Article.find()
		.select('title content thumbnail _id')
		.exec()
		.then(result => {
			const response = {
				count: result.length,
				articles: result.map(article => {
					
					return {
						title: article.title,
						content: article.content,
						_id: article._id,
						thumbnail: {
							type: 'GET',
							url: `http://localhost:3000/${article.thumbnail}`
						}
					}
				})
			}
			res.status(200).json(response);
			
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				message: err
			})
		})
	
});

router.post('/', checklogin, upload.single('articleThumbnail'), function(req, res, next) {
	console.log(req.file)
	const article = new Article({
		_id: new mongoose.Types.ObjectId(),
		title: req.body.title,
		content: req.body.content,
		thumbnail: req.file.path
		
	});
	article
		.save()
		.then(result => {
			// console.log(result);
			res.status(201).json({
				message: 'handling products post request',
				createdarticle: result
			});
			
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			})
			
		})
	
});



module.exports = router;