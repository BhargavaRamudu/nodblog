const express = require('express');
const app = express();

const userroutes = require('./routes/users');
const commentroutes = require('./routes/comments');
const categoryroutes = require('./routes/categories');
const articleroutes = require('./routes/articles');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');

const logging = require('./utils/logging');
const setReqOptions = require('./utils/reqOptions');
const dbConnect = require('./utils/dbConnect');

mongoose.Promise = global.Promise;
require('dotenv').config();
app.use('/uploads', express.static('uploads'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

logging(app);
setReqOptions(app);
dbConnect();
console.log('hello , process has started');

app.use('/articles', articleroutes);
app.use('/users', userroutes);
app.use('/comments', commentroutes);

app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
}) 


app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message
		}
	});
});

console.log('hit the end too');
module.exports = app;