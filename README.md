# Blog API using node js ---- nodblog

This is a demo blog project

## Add environment variable

    MONGO_URL = "your mongodb connection string here"

    It will be used to connect to your mongodb instance on starting the server.

## Install all dependencies:

`Navigate to your project root folder in terminal and install all dependencies using the command` 

`npm install`


## Start the server

    Start the server by using the command `npm run start`